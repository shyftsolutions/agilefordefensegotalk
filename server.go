package main

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
)

func parseFloatArg(parsedQuery url.Values, queryKey string) (float64, error) {
	queryVals, exists := parsedQuery[queryKey]

	if !exists || len(queryVals) != 1 {
		return 0, errors.New("x not set or set multiple times")
	}

	floatVal, err := strconv.ParseFloat(queryVals[0], 64)

	if err != nil {
		return 0, err
	}

	return floatVal, nil
}

func handleErr(w http.ResponseWriter, err error) {
	errMsg := []byte("error!")

	if n, err := w.Write(errMsg); err != nil {
		println(err.Error())
	} else if n != len(errMsg) {
		println("n != len(errMsg)")
	}
}

func AddNumberHandler(w http.ResponseWriter, r *http.Request) {
	parsedQuery := r.URL.Query()

	x, err := parseFloatArg(parsedQuery, "x")

	if err != nil {
		handleErr(w, err)
		return
	}

	y, err := parseFloatArg(parsedQuery, "y")

	if err != nil {
		handleErr(w, err)
		return
	}

	response := []byte(fmt.Sprintf("%f", x+y))

	if n, err := w.Write(response); err != nil {
		println(err.Error())
	} else if n != len(response) {
		println("n != len(response)")
	}

	return
}

func main() {
	http.HandleFunc("/", AddNumberHandler)

	err := http.ListenAndServe(":8080", nil)

	if err != nil {
		println(err.Error())
	}
}
