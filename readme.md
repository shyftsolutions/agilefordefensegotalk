## Introduction

https://bitbucket.org/shyftsolutions/agilefordefensegotalk

This talk is adapted from Rob Pike's talk ["Go at Google: Language Design in the Service of Software Engineering"](https://talks.golang.org/2012/splash.article).

_Code examples from [A Tour of Go](https://tour.golang.org/welcome/1)._

We use Go at Shyft to host performant restful APIs.

## What is Go?

"Go is a compiled, concurrent, garbage-collected, statically typed language developed at Google."

"Go was designed and developed to make working in this environment more productive. Besides its better-known aspects such as built-in concurrency and garbage collection, Go's design considerations include rigorous dependency management, the adaptability of software architecture as systems grow, and robustness across the boundaries between components." 

## Features I like

- Go is, intentionally, a small, simple, language
    - Is anyone _really_ an *expert* in C++?
- **Go is FAST**
    - Nearly instantaneous _ahead of time_ compilation 
        - Entire standard library builds in seconds
    - Memory locality 
    - Garbage collection (escape analysis and stack allocation)
- Go is highly concurrent
    - Goroutines
- Type inference
- First class functions and closures
- Public and private by convention
- Multiple return values
- Pointers, but no pointer arithmetic
- Defined (machine enforceable) formatting
- Unused variables are a compile error

## Things You Might Not Be Expecting

- Interfaces but no inheritance 
    - _Implicit_ interfaces
    - Composition over inheritance anyway
- No Generics _maybe in Go2_
    - Can get the same behavior with interface{}, but not type safe :(
- Error handling without Exceptions
    - Super weird at first, but it helps me pay attention to failure conditions
        - Easier to do the Right Thing, more effort to do the wrong thing
    - How many Exceptions are actually _exceptional_
        - Go has panic and recover for this
    - Works well with multiple return values, zero default values, and type assertions
        - Can check type of returned errors and handle accordingly
        - return Date{}, errors.New("could not parse data")

## Concurrency Model

_"Don't communicate by sharing memory; share memory by communicating."_

- Goroutines are light weight, application level, 'threads'
- Mapped m x n onto OS threads
- Communicate over channels
    - Passing ownership of data between threads avoids many bugs
    - Deadlocks become easier to track down

## Now For Some Code

### Hello World

```
package main

import "fmt"

func main() {
	fmt.Println("Hello world!")
}
```

### Methods and Interfaces

```
package main

import "fmt"

type I interface {
	M()
}

type T struct {
	S string
}

// This method means type T implements the interface I,
// but we don't need to explicitly declare that it does so.
func (t T) M() {
	fmt.Println(t.S)
}

func main() {
	var i I = T{"hello"}
	i.M()
}
```

### Goroutines

```
package main

import (
	"fmt"
	"time"
)

func say(s string) {
	for i := 0; i < 5; i++ {
		time.Sleep(100 * time.Millisecond)
		fmt.Println(s)
	}
}

func main() {
	go say("world")
	say("hello")
}
```

## Live Demo

- ab -c 100 -n 1000 "http://localhost:8080/webpages/login.html?x=10.3&y=12.1"

## Links

- Try Go in your Browser: [The Go Playground](https://play.golang.org/)
- [Go Proverbs](https://go-proverbs.github.io/)
- [A Tour of Go](https://tour.golang.org/welcome/1)
